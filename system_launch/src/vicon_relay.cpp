#include "ros/ros.h"
#include "std_msgs/Float64.h"
#include "std_msgs/Bool.h"
#include "acl_msgs/ViconState.h"
#include "acl_msgs/State.h"
#include "acl_msgs/QuadAHRS.h"
#include "acl_msgs/QuadMode.h"
#include <sensor_msgs/Range.h>
#include <tf2_ros/transform_listener.h>
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"

#include <random>

class RelayNode{
	public:
		ros::NodeHandle nh_p_;
		ros::Publisher pub, pub_state, pub_clock, pub_tf_lidar;
		ros::Subscriber sub, sub_lidar, sub_ahrs, sub_mode, sub_test;
		acl_msgs::ViconState vicon, vicon_old;
		std_msgs::Float64 clock_diff;
		acl_msgs::QuadMode state;
		tf2::Quaternion q;
		double z_w_lidar, z_w_lidar_prev, time_prev, dt, vz, vz_prev, alpha, P, V;
		int counter;
		bool test;
		std::default_random_engine generator;

		RelayNode(const ros::NodeHandle& nh): nh_p_(nh){
			q = tf2::Quaternion::getIdentity();
			counter = 0;
			test = 0;
			time_prev = 0;
			vz_prev = 0;
			z_w_lidar_prev = 0;
			ros::param::param<double>("~filt_const",alpha,0.1);
			ros::param::param<double>("~pose_var",P,0.0);
			ros::param::param<double>("~vel_var",V,0.0);
			sub = nh_p_.subscribe("vicon", 1, &RelayNode::viconCallback, this);
			sub_lidar = nh_p_.subscribe("lidar", 1, &RelayNode::lidarCallback, this);
			sub_ahrs = nh_p_.subscribe("ahrs", 1, &RelayNode::ahrsCallback, this);
			sub_mode = nh_p_.subscribe("mode", 1, &RelayNode::modeCallback, this);
			sub_test = nh_p_.subscribe("test", 1, &RelayNode::testCallback, this);
			pub = nh_p_.advertise<acl_msgs::ViconState>("state_estimate",1);
			pub_state = nh_p_.advertise<acl_msgs::State>("state_vicon",1);
			pub_clock = nh_p_.advertise<std_msgs::Float64>("clock_diff",1);
			pub_tf_lidar = nh_p_.advertise<sensor_msgs::Range>("lidar_tf",1);
		}

		~RelayNode(){}

		void viconCallback(const acl_msgs::ViconState& msg)
		{	
			vicon = msg;
			vicon.header.frame_id = "world";
			clock_diff.data = vicon.header.stamp.toSec() - msg.header.stamp.toSec();

			std::normal_distribution<double> pose_noise(0.0,P);
			std::normal_distribution<double> vel_noise(0.0,V);

			acl_msgs::State state_est;
			state_est.pos.x = msg.pose.position.x + pose_noise(generator);
			state_est.pos.y = msg.pose.position.y + pose_noise(generator);
			state_est.pos.z = msg.pose.position.z + pose_noise(generator);
			state_est.quat.w = msg.pose.orientation.w;
			state_est.quat.x = msg.pose.orientation.x;
			state_est.quat.y = msg.pose.orientation.y;
			state_est.quat.z = msg.pose.orientation.z;
			state_est.vel.x = msg.twist.linear.x + vel_noise(generator);
			state_est.vel.y = msg.twist.linear.y + vel_noise(generator);
			state_est.vel.z = msg.twist.linear.z + vel_noise(generator); 
			state_est.w.x = msg.twist.angular.x;
			state_est.w.y = msg.twist.angular.y;
			state_est.w.z = msg.twist.angular.z;

			vicon.pose.position.x = state_est.pos.x;
			vicon.pose.position.y = state_est.pos.y;
			vicon.pose.position.z = state_est.pos.z;

			vicon.twist.linear.x = state_est.vel.x;
			vicon.twist.linear.y = state_est.vel.y;
			vicon.twist.linear.z = state_est.vel.z;

			// Only for testing filter
			// vicon.pose.position.z = z_w_lidar;
  			// vicon.twist.linear.z = vz;

            if ((state.mode==state.MODE_IDLE || state.mode==state.MODE_WAYPOINT) && !test){
				vicon.header.stamp = ros::Time::now();
				pub.publish(vicon);
				vicon_old.header.stamp = vicon.header.stamp;
				state_est.header.stamp = ros::Time::now();
  				pub_state.publish(state_est);
			}
			pub_clock.publish(clock_diff);
		}

		void lidarCallback(const sensor_msgs::Range& msg){
			if (msg.range!=msg.max_range and counter > 0){
				// Tranform lidar to world frame using quad's onboard attitude estimate
				tf2::Quaternion z_b = tf2::Quaternion(0, 0, msg.range, 0.0);
				// Double check this transform
				tf2::Quaternion z_w = q.inverse()*z_b*q ;
				z_w_lidar = z_w.z();

				// Calculate dt
				dt = msg.header.stamp.toSec() - time_prev;

				// Zero out velocity
				vz = (z_w_lidar - z_w_lidar_prev) / dt;

		        // filter the differentiation
		        double alpha_d = dt / (alpha + dt);
		        vz = vz_prev + alpha_d * (vz - vz_prev);

				sensor_msgs::Range laserReading;

				laserReading.radiation_type = laserReading.INFRARED;
				laserReading.header.frame_id = "lidar";
				laserReading.field_of_view = 0;
				laserReading.min_range = 0;
				laserReading.max_range = 40;

				laserReading.header.stamp = ros::Time::now();   
	            laserReading.range = z_w_lidar;
	            pub_tf_lidar.publish(laserReading);

	            // Publish fake vicon message with lidar-lite measurment for z
	            if (state.mode==state.MODE_ZERO_VEL || state.mode==state.MODE_DESCEND){
	            	vicon.header.frame_id = "world";
	            	// Dont update time stamp to prevent safety fsm from switching back to waypoint mode
					vicon.header.stamp = vicon_old.header.stamp;
					// Only populate vicon message with lidar-lite reading and on-board attitude estimate
					vicon.pose.position.x = 0;
					vicon.pose.position.y = 0;
					vicon.pose.position.z = z_w_lidar;
					
					// q (vicon.pose.orientation.x, vicon.pose.orientation.y, vicon.pose.orientation.z, vicon.pose.orientation.w);

					q.setX(vicon.pose.orientation.x);
					q.setY(vicon.pose.orientation.y);
					q.setZ(vicon.pose.orientation.z);
					q.setW(vicon.pose.orientation.w);
					
					vicon.twist.linear.x = 0;
					vicon.twist.linear.y = 0;
					vicon.twist.linear.z = vz;

					vicon.has_pose = true;
					vicon.has_twist = true;

					vz_prev = vz;

					pub.publish(vicon);
	            }
	            z_w_lidar_prev = z_w_lidar;
		        vz_prev = vz;
		        time_prev = msg.header.stamp.toSec();
		    }
		    counter++;
		    time_prev = msg.header.stamp.toSec();
		}

		void ahrsCallback(const acl_msgs::QuadAHRS& msg){
			q.setX(msg.att.x);
			q.setY(msg.att.y);
			q.setZ(msg.att.z);
			q.setW(msg.att.w);
			// Re-normalize q
			q.normalize();

		}

		void modeCallback(const acl_msgs::QuadMode& msg){
			// Do something with mode
			state = msg;
		}

		void testCallback(const std_msgs::Bool& msg){
			test = msg.data;
		}
};

int main(int argc, char **argv)
{
	ros::init(argc, argv, "vicon_relay", ros::init_options::NoSigintHandler);
	ros::NodeHandle nh_p("~");

	RelayNode node(nh_p);	

	ros::spin();

	return 0;
}
