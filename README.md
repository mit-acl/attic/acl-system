# README #

This repo contains all the custom ROS messages used in the ACL code stack. It also contains the launch files that start the onboard code stack on ACL's vehicles. 

### How do I get set up? ###
* Create an SSH key and add it to your bitbucket account [Explained here](https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html)
```
ssh-keygen
[press enter 3 times for default settings (name of key is ~/.ssh/id_rsa.pub, no password)]
ls -a ~/.ssh           # see that public and private key are in that directory
ssh-agent /bin/bash    # start your ssh agent manually
ssh-add ~/.ssh/id_rsa  # add the newly created key to your ssh agent
ssh-add -l             # just check that the key was added (this lists all the keys your agent knows about)
cat ~/.ssh/id_rsa.pub  # print out your public key.
[copy and paste this whole thing e.g. <ssh-rsa asldfas;dlfkja cpu_name> into bitbucket. go to bitbucket.org, click on the avatar in upper right, select bitbucket settings, select SSH keys, Add Key, type in name of cpu or some description for label, and paste in the entire public key.]
```

* Create a catkin workspace called acl_ws (following the ROS tutorial http://wiki.ros.org/ROS/Tutorials)
```
cd ~/acl_ws/src
git clone <this_repo>
```
* Run ./offBoardSetup.sh to checkout the necessary repos for you personal workstation
```
~/acl_ws/src/acl-system/acl_scripts/scripts/offBoardSetup.sh
```
* To setup a flight computer run ./onBoardSetup.sh instead

### Contribution guidelines ###

* Create a new branch if you want to add a new feature to the code base
```
git checkout -b <branch_name>
```
* Once the feature is complete, create a pull request for a senior student to review your work
```
git add <list files you want to commit>
git commit -m "<commit description>"
git push -u origin <branch_name>
```
* Go on bitbucket, and select "Create a pull request". Select your new branch, to be merged into the master branch, and write a description of what you changed. Submit the request.
* Use the issue tracker if a bug is found in the master branch

### Who do I talk to? ###

* Repo owner or admin
