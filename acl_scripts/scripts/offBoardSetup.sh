#!/usr/bin/env bash
set -e
echo "Installing dependencies."
sudo apt-get install libbluetooth-dev ros-kinetic-joy

echo "Beginning off-board repo setup."
mkdir -p ~/bags
mkdir -p ~/log
cd ~/acl_ws/src
git clone git@bitbucket.org:brettlopez/acl-ros-extra-v2.git -b master
git clone git@bitbucket.org:brettlopez/acl_control.git -b quad_master
# git clone git@bitbucket.org:brettlopez/acl_sensors.git
# git clone git@bitbucket.org:brettlopez/acl_planning.git
cd ~/acl_ws
catkin_make

# See if these lines are in ~/.bashrc, add if not.
LINE='source /opt/ros/kinetic/setup.bash'
FILE=~/.bashrc
grep -q "$LINE" "$FILE" || echo "$LINE" >> "$FILE"
LINE='source ~/acl_ws/devel/setup.bash'
FILE=~/.bashrc
grep -q "$LINE" "$FILE" || echo "$LINE" >> "$FILE"

source ~/.bashrc

rospack list
roscd acl_utils
./setup.py
