#!/usr/bin/env bash
echo "Beginning on-board repo setup."
echo "Type Bitbucket username and press [ENTER]: "
read USERNAME

mkdir -p ~/acl_ws/src
cd ~/acl_ws/src
catkin_init_workspace

git clone https://$USERNAME@bitbucket.org/brettlopez/acl_control.git
git clone https://$USERNAME@bitbucket.org/brettlopez/snap.git -b hex-dev
git clone https://$USERNAME@bitbucket.org/brettlopez/acl-planning.git
cd ~/acl_ws/src/acl-planning/cvx/src/cvxgen 
make -j 4

cd ~/acl_ws

catkin_make

. ~/acl_ws/devel/setup.bash
