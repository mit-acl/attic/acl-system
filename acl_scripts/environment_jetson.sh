#!/usr/bin/env bash
echo "Activating workspace"
# should be cuda-7.0 for TX1, cuda-6.5 for TK1. This will cause caffe to crash with no details if done incorrectly!
export PATH=/usr/local/cuda-7.0/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
export LD_LIBRARY_PATH=/usr/local/cuda-7.0/lib:
export __GL_PERFMON_MODE=1
source ~/catkin_ws/devel/setup.bash
echo "Setup ROS_HOSTNAME"
export ROS_HOSTNAME=$HOSTNAME.local
exec "$@" #Passes arguments. Need this for ROS remote launching to work.


