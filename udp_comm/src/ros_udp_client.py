#!/usr/bin/env python
import rospy
from copy import deepcopy
from acl_msgs.msg import QuadGoal
from Protocol import comm
import time as t


class ROSUDPClient():
	def __init__(self):
		self.node_name = rospy.get_name()

		self.host = rospy.get_param("~clientIP","127.0.0.1") # Receiver IP address
		self.port = rospy.get_param("~clientPort",21567) # Receiver IP address
		self.buf = 4096	

		self.commObject = comm(self.host,self.port)
		self.commObject.bind_port()

		self.goal = QuadGoal()
		# Setup subscriber at the end of the initializer
		self.pub_goal = rospy.Publisher("~goal",QuadGoal,queue_size=1,latch=True)

		self.startUDPClient()

	def startUDPClient(self):
		while not rospy.is_shutdown():
			commData = self.commObject.accept(self.buf)	   
			if commData.id == 'goal':
				# print 'Delay:%s ms id:%s cut_power:%s' %(1000*(t.time()-commData.timestamp),commData.id,commData.data.cut_power)
				self.goal = deepcopy(commData.data)
				self.pub_goal.publish(self.goal)
		self.commObject.close_port()
		rospy.loginfo("[%s] Shutting down." %(self.node_name))			

if __name__ == '__main__':
    rospy.init_node('udp_client_node', anonymous=False)
    node = ROSUDPClient()
