#!/usr/bin/env python
import rospy
from copy import deepcopy
from acl_msgs.msg import QuadGoal
from Protocol import comm

class ROSUDPServer():
	def __init__(self):
		self.node_name = rospy.get_name()

		self.host = rospy.get_param("~serverIP","127.0.0.1") # Receiver IP address
		self.port = rospy.get_param("~serverPort",21567) # Receiver IP address

		self.commObject = comm(self.host,self.port)

		self.goal = QuadGoal()
		# Setup subscriber at the end of the initializer
		self.sub_goal = rospy.Subscriber("~goal",QuadGoal,self.cbNewGoalMsg)


	def cbNewGoalMsg(self,msg):
		self.goal = deepcopy(msg)
		self.commObject.deliver("goal",self.goal)

	def on_shutdown(self):
		self.commObject.close_port()
		rospy.loginfo("[%s] Shutting down." %(self.node_name))

if __name__ == '__main__':
    rospy.init_node('udp_server_node', anonymous=False)
    node = ROSUDPServer()
    rospy.on_shutdown(node.on_shutdown)
    rospy.spin()