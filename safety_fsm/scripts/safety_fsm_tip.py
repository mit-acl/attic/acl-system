#!/usr/bin/env python
import rospy
import numpy as np
from copy import deepcopy
from sensor_msgs.msg import Joy, Range, CameraInfo, PointCloud2
from acl_msgs.msg import CommAge, QuadMode, QuadGoal, BoolStamped, QuadFlightMode, QuadState, JoyDef, QuadAHRS

class SafetyFSMNode():
	def __init__(self):

		self.count = 1

		self.node_name = rospy.get_name()
		self.pub_mode = rospy.Publisher("~mode",QuadMode,queue_size=1,latch=True)
		self.pubEvent = rospy.Publisher("event",QuadFlightMode,queue_size=1,latch=True)

		# Get all params from launch file
		self.safety_fsm_hz = rospy.get_param("~safety_fsm_hz",1)
		self.t_vicon1 = float(rospy.get_param("~safety_fsm_t_vicon1",1))
		self.t_vicon2 = float(rospy.get_param("~safety_fsm_t_vicon2",1))
		assert self.t_vicon1 <= self.t_vicon2

		# Initialize State Machine and publish first state
		self.state = QuadMode()
		self.state.mode = self.state.MODE_IDLE
		self.previousState = QuadMode()
		self.previousState.mode = None
		self.publishMode()

		self.first = False
		self.override = False
		self.proceed = False
		self.did_cut_power = True
		self.dt_vicon = 0.0
		self.start_trigger_received = False
		self.last_trigger_received = BoolStamped()

		# Setup subscriber at the end of the initializer
		self.sub_comm_ages = rospy.Subscriber("~ages",CommAge,self.cbNewAgeMsg)
		self.sub_goal = rospy.Subscriber("~goal",QuadGoal,self.cbNewGoalMsg)
		self.sub_joy = rospy.Subscriber("/joy", Joy, self.joyCB)

		self.flightevent = QuadFlightMode()
		self.prevevent = QuadFlightMode()
		self.joy = JoyDef()
		self.status = QuadState()
		self.status.mode = self.status.NOT_FLYING
		self.refresh = False

		# Ensure all sensors are go
		# self.sensorPubChecker()

		# Setup timer for state machine updates
		self.safety_fsm_period = 1./self.safety_fsm_hz
		self.timer = rospy.Timer(rospy.Duration.from_sec(self.safety_fsm_period),self.cbStateUpdate)

	def numSensorsOnline(self, sensorStatusList):
		return sum([sensor[2] for sensor in sensorStatusList])

	def isSensorOnline(self, topic, msgType):
		try:
			rospy.wait_for_message(topic, msgType, timeout=0.1)
			return True
		except:
			return False


		# List of sensor topics to check, associated message type, and initial status field (should always be False, unless if you want that sensor check skipped for some reason)
		sensorStatusList = [["~lidar", Range, False],
							["~camera/cloud", PointCloud2, False],
							["~camera/rgb/camera_info", CameraInfo, False],
							["~ahrs", QuadAHRS, False]]
		n_sensors = len(sensorStatusList)
		timeout_overall = rospy.Duration(5.0)

	# 	# Fun fact: When using simulated Clock time (e.g., when Gazebo is running), rospy.Time.now() returns time 0 until first message has been received on /clock.
	# 	# So have to wait until it's nonzero, otherwise all timing logic breaks
	# 	start_time = rospy.Time.now()
	# 	while start_time.to_sec()== 0:
	# 		start_time = rospy.Time.now()

		# Stop loop when time runs out or when all sensors statuses are True
		while rospy.Time.now() - start_time < timeout_overall and self.numSensorsOnline(sensorStatusList) < n_sensors:
			for sensorItem in sensorStatusList:
				if sensorItem[2] == False:
					sensorItem[2] = self.isSensorOnline(sensorItem[0], sensorItem[1])

			if self.numSensorsOnline(sensorStatusList) < n_sensors:
				time_left = timeout_overall - (rospy.Time.now() - start_time)
				rospy.loginfo("[safety_fsm_tip][sensorPubChecker] Still waiting for some sensors. Giving up in %fs.",  time_left.to_sec())
	# 			rospy.sleep(0.01)

		if self.numSensorsOnline(sensorStatusList) == n_sensors:
	# 		rospy.loginfo("[safety_fsm_tip][sensorPubChecker] All sensors online! Can proceed!")
	# 	else:
	# 		while 1:
				rospy.loginfo("[safety_fsm_tip][sensorPubChecker] Sensor(s) down! Will not allow launch!")
				for sensorItem in sensorStatusList:
					if sensorItem[2] == False:
						rospy.loginfo("[safety_fsm_tip][sensorPubChecker] %s is not up!", sensorItem[0])


	def cbStateUpdate(self,event):
		self.updateState()

	def updateState(self):

		self.publishMode()

		# rospy.loginfo("[%s] dt_vicon: %s dt_waypt: %s mode: %s" %(self.node_name,self.dt_vicon,self.dt_goal,self.state.mode))
		self.previousState = deepcopy(self.state)
		# If the vehicle cut power and is not in idle then reset
		if self.did_cut_power and self.state.mode != self.state.MODE_IDLE and self.state.mode != self.state.MODE_DESCEND:
			self.start_trigger_received = False
			self.override = False
			self.state.mode = self.state.MODE_IDLE
			self.status.mode = self.status.NOT_FLYING
		elif self.state.mode == self.state.MODE_WAYPOINT:
			# If vicon is old zero velocity and override joy command
			if self.dt_vicon > self.t_vicon1:
				self.state.mode = self.state.MODE_ZERO_VEL
				self.override = True
				self.flightevent.mode = self.flightevent.ESTOP
		elif self.state.mode == self.state.MODE_ZERO_VEL:
			# If re-gain vicon continue
			if self.dt_vicon < self.t_vicon1:
				self.state.mode = self.state.MODE_WAYPOINT
				self.override = False
			# If vicon is old land and override joy command
			elif self.dt_vicon > self.t_vicon2:
				self.state.mode = self.state.MODE_DESCEND
				self.override = True
				self.flightevent.mode = self.flightevent.LAND
		elif self.state.mode == self.state.MODE_IDLE:
			# Special absorbing state, needed to avoid situ where accidental blocking of vicon dots before takeoff would trigger zero velocity/landing maneuvers
			self.override = False
			if self.start_trigger_received:
				if self.dt_vicon < self.t_vicon1:
					self.state.mode = self.state.MODE_WAYPOINT
					self.status.mode = self.status.FLYING
					rospy.loginfo("Taking Off")
					print self.status.mode
					self.flightevent.mode = self.flightevent.TAKEOFF
				else: # no vicon, so ignore the trigger
					self.start_trigger_received = False
					self.proceed = False
					self.override = True
					self.flightevent.mode = self.flightevent.KILL
					self.status.mode = self.status.NOT_FLYING
					self.prevevent = deepcopy(self.flightevent)


		# Publish new flight event if joystick is overriden // fix spam
		if (self.override and self.status.mode != self.previousState.mode and self.flightevent.mode != self.prevevent.mode):
			self.sendEvent()
			self.first = False
		# Publish old flight event to continue mission
		elif not self.first:
			self.first = True
			self.flightevent.mode = deepcopy(self.prevevent.mode)
			self.sendEvent()


	def joyCB(self, data):
		if not self.override:
			# takeoff
			if (data.buttons[self.joy.A] and self.status.mode == self.status.NOT_FLYING):
				self.start_trigger_received = True
				if self.proceed:
					self.status.mode = self.status.FLYING
					rospy.loginfo("Taking Off")
					self.flightevent.mode = self.flightevent.TAKEOFF

			# emergency disable
			elif (data.buttons[self.joy.B] and self.flightevent.mode!=self.flightevent.KILL):
				rospy.loginfo("Kill")
				self.status.mode = self.status.NOT_FLYING
				self.flightevent.mode = self.flightevent.KILL

			# landing
			elif (data.buttons[self.joy.X] and self.status.mode == self.status.FLYING):
				self.status.mode = self.status.NOT_FLYING
				rospy.loginfo("Land")
				self.flightevent.mode = self.flightevent.LAND

			# initialize
			elif (data.buttons[self.joy.CENTER] and self.status.mode == self.status.FLYING and self.flightevent.mode!=self.flightevent.INIT):
				rospy.loginfo("Initialize")
				self.flightevent.mode = self.flightevent.INIT

			# go
			elif (data.buttons[self.joy.START] and self.status.mode == self.status.FLYING and self.flightevent.mode!=self.flightevent.START):
				rospy.loginfo("Start")
				self.flightevent.mode = self.flightevent.START

			# hover
			elif (data.buttons[self.joy.Y] and self.status.mode == self.status.FLYING and self.flightevent.mode!=self.flightevent.ESTOP):
				rospy.loginfo("Hover")
				self.flightevent.mode = self.flightevent.ESTOP

			elif (data.buttons[self.joy.BACK] and not self.refresh):
				self.refresh = True


			if (self.flightevent.mode!=self.prevevent.mode or self.refresh or data.buttons[self.joy.B]):
				self.prevevent.mode = deepcopy(self.flightevent.mode)
				if (self.refresh):
					rospy.logwarn("Re-sending")
				self.refresh = False
				self.sendEvent()

	def sendEvent(self):
		self.flightevent.header.stamp = rospy.get_rostime()
		self.pubEvent.publish(self.flightevent)

	def publishMode(self):
		if self.state.mode != self.previousState.mode:
			self.pub_mode.publish(self.state)
			rospy.logwarn("Mode: %i",self.state.mode)
			self.count = 1
		else:
			self.count = self.count + 1

	def cbNewAgeMsg(self,age_msg):
		self.state.header.stamp = age_msg.header.stamp
		self.dt_vicon = age_msg.vicon_age_secs

	def cbNewGoalMsg(self,msg):
		self.did_cut_power = deepcopy(msg.cut_power)

	def on_shutdown(self):
		rospy.loginfo("[%s] Shutting down." %(self.node_name))

if __name__ == '__main__':
	rospy.init_node('safety_fsm_node', anonymous=False)
	node = SafetyFSMNode()
	rospy.on_shutdown(node.on_shutdown)
	rospy.spin()
